package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyArraysTest {
    //1.
    @Test
    void findMinElementArr() {
        MyArrays myArrays = new MyArrays();
        int actual = myArrays.findMinElementArr(new int[] {1, 2, -5, 0, 6});
        int expected = -5;
        assertEquals(expected, actual);
    }

    //2.
    @Test
    void findMaxElementArr() {
        MyArrays myArrays = new MyArrays();
        int actual = myArrays.findMaxElementArr(new int[] {1, 2, -5, 0, 6});
        int expected = 6;
        assertEquals(expected, actual);
    }

    //3.
    @Test
    void findIndexMinElementArr() {
        MyArrays myArrays = new MyArrays();
        int actual = myArrays.findIndexMinElementArr(new int[] {1, 2, -5, 0, 6});
        int expected = 2;
        assertEquals(expected, actual);
    }

    //4.
    @Test
    void findIndexMaxElementArr() {
        MyArrays myArrays = new MyArrays();
        int actual = myArrays.findIndexMaxElementArr(new int[] {1, 2, -5, 0, 6});
        int expected = 4;
        assertEquals(expected, actual);
    }

    //5.
    @Test
    void findSumElementsNotEvenIndex() {
        MyArrays myArrays = new MyArrays();
        int actual = myArrays.findSumElementsNotEvenIndex(new int[] {1, 2, -5, 0, 6});
        int expected = 2;
        assertEquals(expected, actual);
    }

    //6.
    @Test
    void reverseArray() {
        MyArrays myArrays = new MyArrays();
        String actual = myArrays.reverseArray(new int[] {1, 2, -5, 0, 6});
        String expected = "[6, 0, -5, 2, 1]";
        assertEquals(expected, actual);
    }

    //7.
    @Test
    void calcAmountNotEvenElements() {
        MyArrays myArrays = new MyArrays();
        int actual = myArrays.calcAmountNotEvenElements(new int[] {1, 2, -5, 0, 6});
        int expected = 2;
        assertEquals(expected, actual);
    }

    //8.
    @Test
    void changeSideFirstSecondHalfArr() {
        MyArrays myArrays = new MyArrays();
        String actual = myArrays.changeSideFirstSecondHalfArr(new int[] {1, 2, -5, 0, 6});
        String expected = "[0, 6, -5, 1, 2]";
        assertEquals(expected, actual);
    }

    //9.
    //Bubble
    @Test
    void sortBubble() {
        MyArrays myArrays = new MyArrays();
        String actual = myArrays.sortBubble(new int[] {1, 2, -5, 0, 6});
        String expected = "[-5, 0, 1, 2, 6]";
        assertEquals(expected, actual);
    }
    //Select
    @Test
    void sortSelection() {
        MyArrays myArrays = new MyArrays();
        String actual = myArrays.sortSelection(new int[] {1, 2, -5, 0, 6});
        String expected = "[-5, 0, 1, 2, 6]";
        assertEquals(expected, actual);
    }
    //Insert
    @Test
    void sortInsertion() {
        MyArrays myArrays = new MyArrays();
        String actual = myArrays.sortInsertion(new int[] {1, 2, -5, 0, 6});
        String expected = "[-5, 0, 1, 2, 6]";
        assertEquals(expected, actual);
    }

}