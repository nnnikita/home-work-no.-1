package com.company;

public class Cycles {

    public String findEven() {
        int sum = 0;
        int amount = 0;
        String answer;
        for(int n = 2; n < 99; n += 2) {
            sum += n;
            amount += n / n;
        }
        answer = "Сумма: " + sum + "  Количество: " + amount;
        return answer;
    }

    public String verifySimple (int num) {
        int result = 0;
        for (int i = 2; i <= num; i++) {
            if (num % i == 0) {
                result += 1;
            }
        }
        if (result == 1) {
            return "SIMPLE";
        } else {
            return "NOT simple";
        }
    }

    public double findRoot (int value) {
        double num;
        double root = (double) value / 2;
        do {
            num = root;
            root = (num + (value / num)) / 2;
        }
        while ((num - root) != 0);
        return root;
    }

    public long calculateFactorial (int num) {
        int fact = 1;
        for (int i = 1; i <= num; i++) {
            fact *= i;
        }
        return fact;
    }

    public int summaryNumberChar (int num) {
        int sum = 0;
        while (num != 0) {
            sum = sum + (num % 10);
            num /= 10;
        }
        return sum;
    }

    public int reverseNumber (int num) {
            int reverse = 0;
            int temp;
            while (num > 0) {
                temp = num % 10;
                reverse = reverse * 10 + temp;
                num /= 10;
            }
            return reverse;
        }
}

