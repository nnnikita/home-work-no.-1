package com.company;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Functions {

    //1.
    public String getName(int num) {
        switch (num) {
            case (1):
                return "Monday";
            case (2):
                return "Tuesday";
            case (3):
                return "Wednesday";
            case (4):
                return "Thursday";
            case (5):
                return "Friday";
            case (6):
                return "Saturday";
            case (7):
                return "Sunday";
            default:
                return "Wrong day number";
        }
    }

    //2.
    public double foundDistance(int xa, int ya, int xb, int yb) {
        int legX = xa - xb;
        int legY = ya - yb;
        int value = legX * legX + legY * legY;
        double num;
        double distance = (double) value / 2;
        do {
            num = distance;
            distance = (num + (value / num)) / 2;
        } while ((num - distance) != 0);
        return distance;
    }

    //3.
    public String outputStringNumber(int num) {
        String[] ones = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
        String[] teens = {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
        String[] dozens = {"twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
        String number = "";
        if (num == 0) {
            number = "zero";
        } else if (num > 99) {
            int indexHundreds = num / 100 - 1;
            num %= 100;
            int indexOnes = num % 10 - 1; //одинаковая часть, сделать отдельный метод
            int indexTeens = num % 10;
            int indexDozens = num / 10 - 2;
            if (num < 10 && num > 0) {
                number = ones[indexHundreds] + " hundred " + ones[indexOnes];
            } else if (num > 9 && num < 20) {
                number = ones[indexHundreds] + " hundred " + teens[indexTeens];
            } else if (num >= 20 && num < 100) {
                number = ones[indexHundreds] + " hundred " + dozens[indexDozens] + " " + ones[indexOnes];
            }
        } else if (num < 100 && num > 0) {
            int indexOnes = num % 10 - 1; //одинаковая часть, сделать отдельный метод
            int indexTeens = num % 10;
            int indexDozens = num / 10 - 2;
            if (num < 10) {
                number = ones[indexOnes];
            } else if (num > 9 && num < 20) {
                number = teens[indexTeens];
            } else if (num >= 20 && num < 100) {
                number = dozens[indexDozens] + " " + ones[indexOnes];
            }
        } else {
            number = "Wrong number!";
        }
        return number;
    }

    //4.
    public int outputIntNumber(String input) {
        boolean isValidInput = true;
        int result = 0;
        int finalResult = 0;
        List<String> allowedStrings = Arrays.asList
                (
                        "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen",
                        "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety",
                        "hundred"
                );

        if (input != null && input.length() > 0) {
            input = input.replaceAll("-", " ");
            input = input.toLowerCase().replaceAll(" and", " ");
            String[] splittedParts = input.trim().split("\\s+");

            for (String str : splittedParts) {
                if (!allowedStrings.contains(str)) {
                    isValidInput = false;
                    System.out.println("Invalid word found : " + str);
                    break;
                }
            }
            if (isValidInput) {
                for (String str : splittedParts) {
                    if (str.equalsIgnoreCase("zero")) {
                        result += 0;
                    } else if (str.equalsIgnoreCase("one")) {
                        result += 1;
                    } else if (str.equalsIgnoreCase("two")) {
                        result += 2;
                    } else if (str.equalsIgnoreCase("three")) {
                        result += 3;
                    } else if (str.equalsIgnoreCase("four")) {
                        result += 4;
                    } else if (str.equalsIgnoreCase("five")) {
                        result += 5;
                    } else if (str.equalsIgnoreCase("six")) {
                        result += 6;
                    } else if (str.equalsIgnoreCase("seven")) {
                        result += 7;
                    } else if (str.equalsIgnoreCase("eight")) {
                        result += 8;
                    } else if (str.equalsIgnoreCase("nine")) {
                        result += 9;
                    } else if (str.equalsIgnoreCase("ten")) {
                        result += 10;
                    } else if (str.equalsIgnoreCase("eleven")) {
                        result += 11;
                    } else if (str.equalsIgnoreCase("twelve")) {
                        result += 12;
                    } else if (str.equalsIgnoreCase("thirteen")) {
                        result += 13;
                    } else if (str.equalsIgnoreCase("fourteen")) {
                        result += 14;
                    } else if (str.equalsIgnoreCase("fifteen")) {
                        result += 15;
                    } else if (str.equalsIgnoreCase("sixteen")) {
                        result += 16;
                    } else if (str.equalsIgnoreCase("seventeen")) {
                        result += 17;
                    } else if (str.equalsIgnoreCase("eighteen")) {
                        result += 18;
                    } else if (str.equalsIgnoreCase("nineteen")) {
                        result += 19;
                    } else if (str.equalsIgnoreCase("twenty")) {
                        result += 20;
                    } else if (str.equalsIgnoreCase("thirty")) {
                        result += 30;
                    } else if (str.equalsIgnoreCase("forty")) {
                        result += 40;
                    } else if (str.equalsIgnoreCase("fifty")) {
                        result += 50;
                    } else if (str.equalsIgnoreCase("sixty")) {
                        result += 60;
                    } else if (str.equalsIgnoreCase("seventy")) {
                        result += 70;
                    } else if (str.equalsIgnoreCase("eighty")) {
                        result += 80;
                    } else if (str.equalsIgnoreCase("ninety")) {
                        result += 90;
                    } else if (str.equalsIgnoreCase("hundred")) {
                        result *= 100;
                    }
                }
                finalResult += result;
                result = 0;
            }
        }
        return finalResult;
    }

}

