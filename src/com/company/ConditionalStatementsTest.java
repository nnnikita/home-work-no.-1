package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConditionalStatementsTest {

    @Test
    void checkParity() {
        ConditionalStatements conditionalStatements = new ConditionalStatements();
        int actual = conditionalStatements.checkParity(4,4);
        int expected = 16;
        assertEquals(expected,actual);
    }

    @Test
    void defineQuarter() {
        ConditionalStatements conditionalStatements = new ConditionalStatements();
        int actual = conditionalStatements.defineQuarter(-1,1);
        int expected = 2;
        assertEquals(expected,actual);
    }

    @Test
    void findPositiveNumberSum() {
        ConditionalStatements conditionalStatements = new ConditionalStatements();
        int actual = conditionalStatements.findPositiveNumberSum(-5,5,6);
        int expected = 11;
        assertEquals(expected,actual);
    }

    @Test
    void calculateMaxSumOrProduct() {
        ConditionalStatements conditionalStatements = new ConditionalStatements();
        int actual = conditionalStatements.calculateMaxSumOrProduct(1,3,4);
        int expected = 15;
        assertEquals(expected,actual);
    }

    @Test
    void defineMark() {
        ConditionalStatements conditionalStatements = new ConditionalStatements();
        String actual = conditionalStatements.defineMark(80);
        String expected = "B";
        assertEquals(expected,actual);
    }
}