package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FunctionsTest {

    //1.
    @Test
    void getName() {
        Functions functions = new Functions();
        String actual = functions.getName(2);
        String expected = "Tuesday";
        assertEquals(expected, actual);
    }

    //2.
    @Test
    void foundDistance() {
        Functions functions = new Functions();
        double actual = functions.foundDistance(3, 4, -4, 4);
        double expected = 7;
        assertEquals(expected,actual);
    }

    //3.
    @Test
    void outputStringNumber() {
        Functions functions = new Functions();
        String actual = functions.outputStringNumber(19);
        String expected = "nineteen";
        assertEquals(expected,actual);
    }

    //4.
    @Test
    void outputIntNumber() {
        Functions functions = new Functions();
        int actual = functions.outputIntNumber("ninety seven");
        int expected = 97;
        assertEquals(expected,actual);
    }

}