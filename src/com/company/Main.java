package com.company;

public class Main {

    public static void main(String[] args) {

        //Условные операторы

        ConditionalStatements conditionalStatements = new ConditionalStatements();

        //1.Если а – четное посчитать а*б, иначе а+б
        System.out.println(conditionalStatements.checkParity(4, 9));

        //2.Определить какой четверти принадлежит точка с координатами (х,у)
        System.out.println(conditionalStatements.defineQuarter(1, 1));

        //3.Найти суммы только положительных из трех чисел
        System.out.println(conditionalStatements.findPositiveNumberSum(1, 3, -2));

        //4.Посчитать выражение (макс(а*б*с, а+б+с))+3
        System.out.println(conditionalStatements.calculateMaxSumOrProduct(2,0,4));

        //5.Написать программу определения оценки студента по его рейтингу
        System.out.println(conditionalStatements.defineMark(79));


        //Цыклы

        Cycles cycles = new Cycles();

        //1.Найти сумму четных чисел и их количество в диапазоне от 1 до 99
        System.out.println(cycles.findEven());

        //2.Проверить простое ли число? (число называется простым, если оно делится только само на себя и на 1)
        System.out.println(cycles.verifySimple(13));

        //3.Найти корень натурального числа с точностью до целого (рассмотреть вариант последовательного подбора и метод бинарного поиска)
        System.out.println(cycles.findRoot(4));

        //4.Вычислить факториал числа n. n! = 1*2*…*n-1*n;
        System.out.println(cycles.calculateFactorial(12));

        //5.Посчитать сумму цифр заданного числа
        System.out.println(cycles.summaryNumberChar(6508));

        //6.Вывести число, которое является зеркальным отображением последовательности цифр заданного числа, например, задано число 123, вывести 321.
        System.out.println(cycles.reverseNumber(123));


        //Массивы

        MyArrays myArrays = new MyArrays();

        //Инициализируем масив который будем передавать в наши методы
        int[] myArray = new int[]{20, 5, -48, 301, 55, 0, -7, 5, 6, 66, 47, -88, 21, -3};

        //1.Найти минимальный элемент массива

        System.out.println(myArrays.findMinElementArr(myArray));
//        System.out.println(myArrays.findMinElementArr(new int[] {24, -56, 3, -2}));

        //2. Найти максимальный элемент массива
        System.out.println(myArrays.findMaxElementArr(myArray));

        //3. Найти индекс минимального элемента массива
        System.out.println(myArrays.findIndexMinElementArr(myArray));

        //4. Найти индекс максимального элемента массива
        System.out.println(myArrays.findIndexMaxElementArr(myArray));

        //5. Посчитать сумму элементов массива с нечетными индексами
        System.out.println(myArrays.findSumElementsNotEvenIndex(myArray));

        //6. Сделать реверс массива (массив в обратном направлении)
        System.out.println(myArrays.reverseArray(myArray));

        //7. Посчитать количество нечетных элементов массива
        System.out.println(myArrays.calcAmountNotEvenElements(myArray));


        //8. Поменять местами первую и вторую половину массива, например, для массива 1 2 3 4, результат 3 4 1 2
        System.out.println(myArrays.changeSideFirstSecondHalfArr(myArray));

        //9. Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert))
        System.out.println(myArrays.sortBubble(myArray));
        System.out.println(myArrays.sortSelection(myArray));
        System.out.println(myArrays.sortInsertion(myArray));


        //Функции

        Functions functions = new Functions();

        //1. Получить строковое название дня недели по номеру дня.

        System.out.println(functions.getName(6));

        //2.Найти расстояние между двумя точками в двухмерном декартовом пространстве.
        System.out.println(functions.foundDistance(1,-25,-56,48));

        //3.Вводим число(0-999), получаем строку с прописью числа.
        System.out.println(functions.outputStringNumber(5));

        //4.Вводим строку, которая содержит число, написанное прописью (0-999). Получить само число
        System.out.println(functions.outputIntNumber("Seven hundred fifty four"));

    }
}
