package com.company;

public class ConditionalStatements {

    public int checkParity(int a, int b) {
        int res;
        if (a % 2 == 0) {
            res = a * b;
        } else {
            res = a + b;
        }
        return res;
    }

    public int defineQuarter(int x, int y) {
        int quarter;
        if (x > 0 && y > 0) {
            quarter = 1;
        } else if (x < 0 && y > 0) {
            quarter = 2;
        } else if (x < 0 && y < 0) {
            quarter = 3;
        } else if (x > 0 && y < 0) {
            quarter = 4;
        } else {
            quarter = 0;
        }
        return quarter;
    }

    public int findPositiveNumberSum (int a, int b, int c) {
        int sum;
        if (a < 0) {
            a = 0;
        }
        if (b < 0) {
            b = 0;
        }
        if (c < 0) {
            c = 0;
        }
        sum = a + b + c;
        return sum;
    }

    public int calculateMaxSumOrProduct (int a, int b, int c) {
        int x;
        int y;
        x = a * b * c;
        y = a + b + c;
        int result = x >= y ? (x + 3) : (y + 3);
        return result;
    }

    public String defineMark (int rating) {
        String mark;
        if (rating >= 0 && rating < 20) {
            mark = "F";
        } else if (rating >= 20 && rating < 40) {
            mark = "E";
        } else if (rating >= 40 && rating < 60) {
            mark = "D";
        } else if (rating >= 60 && rating < 75) {
            mark = "C";
        } else if (rating >= 75 && rating < 90) {
            mark = "B";
        } else if (rating >= 90 && rating <= 100) {
            mark = "A";
        } else {
            mark = "Wrong mark!";
        }
        return mark;
    }
}