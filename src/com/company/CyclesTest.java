package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CyclesTest {

    @Test
    void findEven() {
        Cycles cycles = new Cycles();
        String actual = cycles.findEven();
        String expected = "Сумма: 2450  Количество: 49";
        assertEquals(expected,actual);
    }

    @Test
    void verifySimple() {
        Cycles cycles = new Cycles();
        String actual = cycles.verifySimple(9);
        String expected = "NOT simple";
        assertEquals(expected,actual);
    }

    @Test
    void findRoot() {
        Cycles cycles = new Cycles();
        double actual = cycles.findRoot(9);
        double expected = 3;
        assertEquals(expected,actual);
    }

    @Test
    void calculateFactorial() {
        Cycles cycles = new Cycles();
        long actual = cycles.calculateFactorial(5);
        long expected = 120;
        assertEquals(expected,actual);
    }

    @Test
    void sumNumberChar() {
        Cycles cycles = new Cycles();
        int actual = cycles.summaryNumberChar(1504);
        int expected = 10;
        assertEquals(expected,actual);
    }

    @Test
    void reverseNumber() {
        Cycles cycles = new Cycles();
        int actual = cycles.reverseNumber(1905);
        int expected = 5091;
        assertEquals(expected,actual);
    }
}