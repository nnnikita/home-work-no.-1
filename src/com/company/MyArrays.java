package com.company;
import java.util.Arrays;

public class MyArrays {

    //1.
    public int findMinElementArr(int[] array) {
        int min = array[0];
        for (int x : array) {
            if (x < min) min = x;
        }
//        for (int i = 1; i < array.length; i++) {
//            min = Math.min(min, array[i]);
//        }
        return min;
    }

    //2.
    public int findMaxElementArr(int[] array) {
        int max = array[0];
        for (int x : array) {
            if (x > max) max = x;
        }
//        for (int i = 1; i < array.length; i++) {
//            max = Math.max(max, array[i]);
//        }
        return max;
    }

    //3.
    public int findIndexMinElementArr(int[] array) {
        int indexMin = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] < array[indexMin]) {
                indexMin = i;
            }
        }
        return indexMin;
    }

    //4.
    public int findIndexMaxElementArr(int[] array) {
        int indexMax = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] > array[indexMax]) {
                indexMax = i;
            }
        }
        return indexMax;
    }

    //5.
    public int findSumElementsNotEvenIndex(int[] array) {
        int sumElement = 0;
        for (int i = 0; i < array.length; i++) {
            if (i % 2 != 0) {
                sumElement += array[i];
            }
        }
        return sumElement;
    }

    //6.
    public String reverseArray(int[] array) {
        String reverseArray;
        int size = array.length;
        for (int i = 0; i < size / 2; i++) {
            int temp = array[i];
            array[i] = array[size - 1 - i];
            array[size - 1 - i] = temp;
        }
        reverseArray = Arrays.toString(array);
        return reverseArray;
    }

    //7.
    public int calcAmountNotEvenElements(int[] array) {
        int amount = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 != 0) {
                amount++;
            }
        }
        return amount;
    }

    //8.
    public String changeSideFirstSecondHalfArr(int[] array) {
        String changeHalf;
        int size = array.length;
        int att = array.length % 2;
        for (int i = 0; i < size / 2; i++) {
            int temp = array[i];
            array[i] = array[size / 2 + i + att];
            array[size / 2 + i + att] = temp;
        }
        changeHalf = Arrays.toString(array);
        return changeHalf;
    }

    //9.
    public String sortBubble(int[] array) {
        boolean needIteration = true;
        while (needIteration) {
            needIteration = false;
            for (int i = 1; i < array.length; i++) {
                if (array[i] < array[i - 1]) {
                    int temp = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = temp;
                    needIteration = true;
                }
            }
        }
        return Arrays.toString(array);
    }

    public String sortSelection(int[] array) {
        for (int min = 0; min < array.length-1; min++) {
            int least = min;
            for (int j = min + 1; j < array.length; j++) {
                if (array[j] < array[least]) {
                    least = j;
                }
            }
            int temp = array[min];
            array[min] = array[least];
            array[least] = temp;
        }
        return Arrays.toString(array);
    }

    public String sortInsertion (int [] array) {
        for (int left = 0; left < array.length; left++) {
            int value = array[left];
            int i = left - 1;
            for (; i >= 0; i--) {
                if (value < array[i]) {
                    array[i + 1] = array[i];
                } else {
                    break;
                }
            }
            array[i + 1] = value;
        }
        return Arrays.toString(array);
    }

}






